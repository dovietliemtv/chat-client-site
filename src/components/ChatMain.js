import React, { Component} from 'react';
import MessagesList from './MessagesList';
import avatar from '../images/avatar.jpg';
import SendMessageForm from './SendMessageForm';

const DUMMY_DATA  = [
    {
        senderId: 1,
        iscurrentUser: true,
        isListChat: false,
        senderName: 'liemdotv',
        text: "who'll win?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:00 PM',
    },
    {
        senderId: 1,
        iscurrentUser: true,
        isListChat: true,
        senderName: 'liemdotv',
        text: "hahahaha?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: false,
        senderName: 'ha ho',
        text: "i win",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: true,
        senderName: 'liemdotv',
        text: "hahahaha?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: false,
        senderName: 'ha ho',
        text: "i win",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 1,
        iscurrentUser: true,
        isListChat: true,
        senderName: 'liemdotv',
        text: "hahahaha?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: false,
        senderName: 'ha ho',
        text: "i win",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 1,
        iscurrentUser: true,
        isListChat: true,
        senderName: 'liemdotv',
        text: "hahahaha?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: false,
        senderName: 'ha ho',
        text: "i win",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 1,
        iscurrentUser: true,
        isListChat: true,
        senderName: 'liemdotv',
        text: "hahahaha?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: false,
        senderName: 'ha ho',
        text: "i win",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 1,
        iscurrentUser: true,
        isListChat: true,
        senderName: 'liemdotv',
        text: "hahahaha?",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    },
    {
        senderId: 2,
        iscurrentUser: false,
        isListChat: false,
        senderName: 'ha ho',
        text: "i win",
        avatar: avatar,
        date: '10/9/2018',
        time: '10:01 PM',
    }
  ];
export default class ChatMain extends Component{
    constructor(props){
        super(props);
        this.state = {
            messages: DUMMY_DATA
         }
    }
    render(){
        return(
            <div className="chat-b-chatmain">
                <div className="chat-b-chatmain-inner">
                    <div className="row-fluid chat-b-head">
                        <div className="span-6 chat-left">
                            <h3>An Nguyen</h3>
                            <div className="chat-b-left-option">
                                <button className="btn-image">Gallery</button>
                                <span className="chat-activer-now">Last seen 7min ago</span>
                            </div>
                        </div>
                        <div className="span-6 chat-right">
                            <button className="btn-round btn-video-call"></button>
                            <button className="btn-round btn-phone-call"></button>
                            <button className="btn-round btn-add-friend"></button>
                        </div>
                    </div>
                    <div className="chat-b-body">
                        <div className="chat-b-body-inner chat-scroll-bar">
                            <div className="chat-date">today</div>
                            <MessagesList messages={this.state.messages} />
                        </div>
                    </div>
                    <div className="chat-b-foot">
                        <div className="chat-b-foot-inner">
                            <SendMessageForm />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}