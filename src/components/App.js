import React, {Component} from 'react';
import Search from './Search';
import UserChatBoxPanel from './UserChatBoxPanel';
import UserInfo from './UserInfo';
import ChatMain from './ChatMain';

class App extends Component {
    showListFriend = () =>{
        let body = document.getElementsByTagName('body')[0];
        body.classList.add('chat-show-b-left');
    }

    render() {  
      return (
        <div>
            <header className='desktop-hide'>
                <div className='chat-b-container'>
                    <button className='btn-back-arrow' onClick={this.showListFriend}></button>
                    <p>Olab chat</p>
                </div>
            </header>
            <section className='chat-s-app'>
                <div className='chat-b-container'>
                    <div className='chat-b-left'>
                        <div className='chat-b-left-inner' id='toggle-list-friend'>
                            <div className='chat-b-head'>
                                <UserInfo />
                            </div>
                            <div className='chat-b-body'>
                                <Search />
                            </div>
                            <div className='chat-b-foot chat-scroll-bar'>
                                <UserChatBoxPanel />
                            </div>
                        </div>
                    </div>
                    <div className='chat-b-right'>
                        <div className='chat-b-right-inner'>
                            <ChatMain />
                        </div>
                    </div>
                </div>
            </section>
        </div>

      );
    }
  }
export default App;