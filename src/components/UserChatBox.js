import React, {Component} from 'react';
import avatar from '../images/avatar.jpg'
export default class UserChatBox extends Component {
    constructor(){
        super();
        this.state = {
            avatar: avatar,
            username: 'John Appleseed',
            numMessage: 1,
            newMeassage: 'Lorem ipsum dolor sit',
        }
    }
    render() {
        return (
            <div className='chat-b-user-box'>
                <div className='chat-left'>
                    <img src={this.state.avatar} alt="avatar"/>
                    { this.state.numMessage !== 0 ?
                        <span> {this.state.numMessage} </span>
                        : ''
                    }
                </div>
                <div className='chat-center'>
                    <p>{this.state.username}</p>
                    <p>{this.state.newMeassage}</p>
                </div>
                <div className='chat-right'>
                    <p>2pm</p>
                </div>
            </div>
        );
    }
  }