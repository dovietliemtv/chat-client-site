import React, {Component} from 'react';
import avatar from '../images/avatar.jpg'

export default class UserInfo extends Component {
    render() {
      return (
        <div className="chat-b-user-info">
            <div className='chat-left'>
                <div className='chat-head'>
                    <img src={avatar} alt="avatar"/>
                </div>
                <div className='chat-body'>
                    <p>liemdotv</p>
                    <div className='chat-b-status'>
                        <a href="#">Active</a>
                        <ul className='chat-dropdown'>
                            <li>                   
                                <a href="#">Active</a>
                            </li>
                            <li>                   
                                <a href="#">Do not disturb</a>
                            </li>
                            <li>                   
                                <a href="#">Invisible</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div className='chat-right'>
                <a href="#" className='chat-btn-dot'>
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
                <ul className='chat-dropdown'>
                    <li>                   
                        <a href="#">Setting</a>
                    </li>
                    <li>                   
                        <a href="#">logout</a>
                    </li>
                </ul>
            </div>
           
        </div>
      );
    }
}
