import React, {Component} from 'react';
export default class Search extends Component {
    render() {
      return (
        <div className="chat-b-search">
            <input placeholder='Search' />
            <button className="btn-search">Search</button>
        </div>
      );
    }
}
