import React, {Component} from 'react';
import UserChatBox from './UserChatBox';
export default class UserChatBoxPanel extends Component {
    constructor(props){
        super(props);
    }
    render() {
        const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        const listItems = numbers.map((number) =>
            <UserChatBox key={number.toString()} />
        );
        return (
          <div className="chat-b-current-chat-box-panel">
              {listItems}
          </div>
        );
    }
  }