import React, { Component} from 'react';

export default class Message extends Component{
    constructor(props){
        super(props);
    }
    render(){
        const className = [
            'chat-b-box',
            this.props.message.iscurrentUser ? 'is-current-user' : '',
            this.props.message.isListChat ? 'is-list-chat' : '',
        ]
        return(
            <div className={ className.join(" ").trim() }>
                <div className="chat-avatar">
                    <img src={this.props.message.avatar}/>
                </div>
                <div className="chat-body">
                    <span> {this.props.message.senderName}, {this.props.message.time}</span>
                    <div className="chat-text">
                        <p>{this.props.message.text}</p>
                    </div>
                </div>
            </div>
        );
    }
}