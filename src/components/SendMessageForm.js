import React, {Component} from 'react';
export default class SendMessageForm extends Component {
    render() {
      return (
        <form className="send-message-form">
            <input placeholder="Type a message" type="text" />
            <div className="chat-attach-file">
                <button className="btn-attach btn-img"></button>
                <input type="file" accept="image/*" ref="input" />
            </div>
            <div className="chat-attach-file">
            <button className="btn-attach btn-file"></button>
                <input type="file" ref="input" />
            </div>
            <button type="submit" className="btn-submit can-submit"></button>
        </form>
      );
    }
}
