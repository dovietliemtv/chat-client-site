import React, {Component} from 'react';

export default class AddFriend extends Component{
    render() {
        return (
            <div className='chat-b-addfriend'>
                <input placeholder='Friend'/>
                <button className='chat-btn'>New</button>
            </div>
        )
    }
}