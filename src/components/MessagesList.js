import React, { Component} from 'react';
import Message from './Message';

export default class MessagesList extends Component{

    constructor(props){
        super(props);
        
    }
    render(){
        
        return(
            <div className="chat-b-list-messages">
                 {
                     this.props.messages.map((message, i)  => {
                         return(
                            <Message key={i}  message = {message}/>
                         )
                     })
                 }
            </div>
        );
    }
}